package com.example.smsmessenger.notification;

import java.util.Locale;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

public class TTSEngine implements OnInitListener {

	TextToSpeech tts;
	String message = "";

	public TTSEngine(Context context, String message) {
		super();
		tts = new TextToSpeech(context, this);
		this.message = message;
	}

	public void read(String msg) {
		tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
	}

	public void release() {
		tts.shutdown();
	}

	@Override
	public void onInit(int status) {
		tts.setLanguage(Locale.US);
		
		read(message);
	}

}
