package com.example.smsmessenger.notification;

import android.content.Context;
import android.widget.Toast;

/**
 * @author TR1-03
 *
 */
public class Utils {
	
	/**
	 * @param message
	 */
	public static void notify(Context context, String message) {
		TTSEngine tts = new TTSEngine(context, message);
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
