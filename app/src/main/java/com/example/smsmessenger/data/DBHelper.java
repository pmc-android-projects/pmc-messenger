package com.example.smsmessenger.data;

import java.util.ArrayList;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Paul Mark Castillo
 * 
 */
public class DBHelper extends SQLiteOpenHelper {
	/**
	 * 
	 */
	String TAG = "event";
	/**
	 * 
	 */
	SQLiteDatabase db;

	/**
	 * @param context
	 */
	public DBHelper(Context context) {
		this(context, "MyDB", null, 1);
	}

	/**
	 * @param context
	 * @param name
	 * @param factory
	 * @param version
	 */
	public DBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "Creating Database...");
		String contactsTable = "CREATE TABLE tblContacts (contactid INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, mobile TEXT, email TEXT)";
		String messsagesTable = "CREATE TABLE tblMessages (messageid INTEGER PRIMARY KEY AUTOINCREMENT, contactid INTEGER, message TEXT);";

		db.execSQL(contactsTable);
		db.execSQL(messsagesTable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	/**
	 * 
	 */
	public void openDB() {
		db = getWritableDatabase();
	}

	/**
	 * 
	 */
	public void closeDB() {
//		close();
	}
	
	/**
	 * @return
	 */
	public ArrayList<Contact> getContacts() {
		openDB();
		ArrayList<Contact> contactList = new ArrayList<Contact>();
		
		Cursor c = db.query("tblContacts", null, null, null, null, null, null);
		
		if (c.moveToFirst()) {
			do {
				Contact contact = new Contact();
				
				contact.setContactID(c.getInt(0));
				contact.setName(c.getString(1));
				contact.setMobile(Long.parseLong(c.getString(2)));
				contact.setEmail(c.getString(3));
				
				contactList.add(contact);
			} while (c.moveToNext());
		}
		
		closeDB();
		return contactList;
	}

	/**
	 * @param contact
	 */
	public long addContact(Contact contact) {
		openDB();
		
		ContentValues initialValues = new ContentValues();
		initialValues.put("name", contact.getName());
		initialValues.put("mobile", "" + contact.getMobile());
		initialValues.put("email", contact.getEmail());
		long rowID = db.insert("tblContacts", null, initialValues);
		
		closeDB();
		return rowID;
	}

	/**
	 * @param contact
	 */
	public void deleteContact(Contact contact) {
		db.delete("tblContacts",
				"name = '" + contact.getName() + "'",
				null);
	}
	
	/**
	 * @return
	 */
	public ArrayList<Message> getMessages(Contact contact) {
		Log.d(TAG, "Searching messages for contactID: " + contact.getContactID());
		openDB();
		ArrayList<Message> messagesList = new ArrayList<Message>();
		
		Cursor c = db.query("tblMessages", null, "contactid=" + contact.getContactID() + "", null, null, null, null);
		
		if (c.moveToFirst()) {
			do {
				Message message = new Message();
				
				message.setMessageID(c.getInt(0));
				message.setMessage(c.getString(2));
				
				messagesList.add(message);
			} while (c.moveToNext());
		}
		
		closeDB();
		return messagesList;
	}
	
	/**
	 * @param messsage
	 */
	public long addMessage(Contact contact, Message message) {
		openDB();
		
		ContentValues initialValues = new ContentValues();
		initialValues.put("contactid", "" + contact.getContactID());
		initialValues.put("message", message.getMessage());
		long rowID = db.insert("tblMessages", null, initialValues);
		
		closeDB();	
		return rowID;
	}
	
	/**
	 * @param message
	 */
	public void deleteMessage(Message message) {
		db.delete("tblMessages",
				"messageid = '" + message.getMessageID() + "'",
				null);		
	}
}
