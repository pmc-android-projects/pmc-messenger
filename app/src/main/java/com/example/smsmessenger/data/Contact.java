package com.example.smsmessenger.data;

/**
 * @author TR1-03
 * 
 */
public class Contact {
	/**
	 * 
	 */
	private long contactID = -1;
	/**
	 * 
	 */
	private String email = "";
	/**
	 * 
	 */
	private long mobile = 0;
	/**
	 * 
	 */
	private String name = "";
	
	/**
	 * @return the contactID
	 */
	public long getContactID() {
		return contactID;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the mobile
	 */
	public long getMobile() {
		return mobile;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param contactID the contactID to set
	 */
	public void setContactID(long contactID) {
		this.contactID = contactID;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getName();
	}
}
