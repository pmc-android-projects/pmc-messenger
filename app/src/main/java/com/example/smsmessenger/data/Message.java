package com.example.smsmessenger.data;

/**
 * @author TR1-03
 * 
 */
public class Message {
	/**
	 * 
	 */
	String message = "";
	/**
	 * 
	 */
	private long messageID = -1;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the messageID
	 */
	public long getMessageID() {
		return messageID;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param messageID the messageID to set
	 */
	public void setMessageID(long messageID) {
		this.messageID = messageID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getMessage();
	}
}
