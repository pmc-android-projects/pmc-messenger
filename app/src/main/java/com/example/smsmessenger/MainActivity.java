package com.example.smsmessenger;

import java.util.ArrayList;

import com.example.smsmessenger.data.Contact;
import com.example.smsmessenger.data.DBHelper;
import com.example.smsmessenger.notification.Utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;

/**
 * @author TR1-03
 * 
 */
public class MainActivity extends Activity implements OnClickListener,
		OnItemClickListener, OnItemLongClickListener, TextWatcher {
	/**
	 * 
	 */
	String TAG = "event";
	/**
	 * 
	 */
	ArrayList<Contact> arrayList;
	/**
	 * 
	 */
	ArrayAdapter<Contact> adapter;
	/**
	 * 
	 */
	EditText etName;
	/**
	 * 
	 */
	EditText etMobile;
	/**
	 * 
	 */
	EditText etEmail;
	/**
	 * 
	 */
	Button btAddContact;
	/**
	 * 
	 */
	ListView lvContacts;
	/**
	 * 
	 */
	DBHelper db;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		db = new DBHelper(getApplicationContext());
		arrayList = db.getContacts();

		// arrayList = new ArrayList<Contact>();
		adapter = new ArrayAdapter<Contact>(this,
				android.R.layout.simple_expandable_list_item_1, arrayList);

		etName = (EditText) findViewById(R.id.etName);
		etMobile = (EditText) findViewById(R.id.etMobile);
		etEmail = (EditText) findViewById(R.id.etEmail);

		etName.addTextChangedListener(this);
		etMobile.addTextChangedListener(this);
		etEmail.addTextChangedListener(this);

		btAddContact = (Button) findViewById(R.id.btAddContact);
		btAddContact.setOnClickListener(this);

		lvContacts = (ListView) findViewById(R.id.lvContacts);
		lvContacts.setAdapter(adapter);
		lvContacts.setOnItemClickListener(this);
		lvContacts.setOnItemLongClickListener(this);

		validateAddButton();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (v == btAddContact) {
			Contact contact = new Contact();
			contact.setName(etName.getText().toString());
			contact.setMobile(Long.parseLong(etMobile.getText().toString()));
			contact.setEmail(etEmail.getText().toString());

			long contactID = db.addContact(contact);
			contact.setContactID(contactID);
			
			adapter.add(contact);
			adapter.notifyDataSetChanged();
			resetAddContact();

			Utils.notify(this, "Contact successfully added.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemLongClickListener#onItemLongClick(android
	 * .widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		final Contact contact = adapter.getItem(arg2);

		PopupMenu menu = new PopupMenu(this, arg1);
		menu.inflate(R.menu.contact);

		menu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.mnuCallContact:
					callContact(contact);
					break;
				case R.id.mnuDeleteContact:
					deleteContact(contact);
					break;
				case R.id.mnuEmailContact:
					emailContact(contact);
					break;
				case R.id.mnuViewContact:
					viewContact(contact);
					break;
				}
				return true;
			}
		});
		menu.show();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget
	 * .AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Contact contact = adapter.getItem(arg2);

		Intent i = new Intent(MainActivity.this, SendSMSActivity.class);
		i.putExtra("id", contact.getContactID());
		i.putExtra("name", contact.getName());
		i.putExtra("mobile", contact.getMobile());
		i.putExtra("email", contact.getEmail());

		Log.d(TAG, "Sending Extras: name=" + contact.getName() + ";mobile="
				+ contact.getMobile() + ";id=" + contact.getContactID());
		startActivity(i);
	}

	/**
	 * @param contact
	 */
	private void callContact(final Contact contact) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + contact.getMobile()));
		startActivity(callIntent);
	}

	/**
	 * @param contact
	 */
	private void deleteContact(final Contact contact) {
		adapter.remove(contact);
		adapter.notifyDataSetChanged();
		db.deleteContact(contact);

		Utils.notify(this, "Contact successfully deleted.");
	}

	/**
	 * @param contact
	 */
	private void emailContact(final Contact contact) {
		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL, new String[] { contact.getEmail() });
		email.setType("message/rfc822");

		startActivity(Intent.createChooser(email, "Select Email client: "));
	}

	/**
	 * @param contact
	 */
	private void viewContact(final Contact contact) {
		Intent i = new Intent(MainActivity.this, ContactProfileActivity.class);
		i.putExtra("id", contact.getContactID());
		i.putExtra("name", contact.getName());
		i.putExtra("mobile", contact.getMobile());
		i.putExtra("email", contact.getEmail());
		startActivity(i);
	}

	/**
	 * @param arg0
	 */
	@Override
	public void afterTextChanged(Editable arg0) {
		validateAddButton();
	}

	/**
	 * @param s
	 * @param start
	 * @param count
	 * @param after
	 */
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		validateAddButton();
	}

	/**
	 * @param s
	 * @param start
	 * @param before
	 * @param count
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		validateAddButton();
	}

	/**
	 * 
	 */
	public void validateAddButton() {
		String name = etName.getText().toString();
		String mobile = etMobile.getText().toString();
		String email = etEmail.getText().toString();

		if (name == null || name.trim().equals("") || mobile == null
				|| mobile.trim().equals("") || email == null
				|| email.trim().equals("")) {
			btAddContact.setEnabled(false);
		} else {
			btAddContact.setEnabled(true);
		}
	}

	/**
	 * 
	 */
	public void resetAddContact() {
		etName.setText("");
		etMobile.setText("");
		etEmail.setText("");
		btAddContact.setEnabled(false);
	}
}
