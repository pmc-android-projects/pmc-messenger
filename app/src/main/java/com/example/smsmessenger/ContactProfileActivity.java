package com.example.smsmessenger;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ContactProfileActivity extends Activity {
	/**
	 * 
	 */
	TextView tvNameProfile;
	/**
	 * 
	 */
	TextView tvMobileProfile;
	/**
	 * 
	 */
	TextView tvEmailProfile;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_profile);

		tvNameProfile = (TextView) findViewById(R.id.tvNameProfile);
		tvMobileProfile = (TextView) findViewById(R.id.tvMobileProfile);
		tvEmailProfile = (TextView) findViewById(R.id.tvEmailProfile);

		Bundle b = getIntent().getExtras();

		String name = b.getString("name");
		long mobile = b.getLong("mobile");
		String email = b.getString("email");

		tvNameProfile.setText(name);
		tvMobileProfile.setText("" + mobile);
		tvEmailProfile.setText(email);
	}
}
