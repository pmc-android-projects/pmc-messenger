package com.example.smsmessenger;

import java.util.ArrayList;

import com.example.smsmessenger.data.Contact;
import com.example.smsmessenger.data.DBHelper;
import com.example.smsmessenger.data.Message;
import com.example.smsmessenger.notification.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SendSMSActivity extends Activity implements OnClickListener,
		OnItemLongClickListener {
	/**
	 * 
	 */
	String TAG = "event";
	/**
	 * 
	 */
	ArrayList<Message> arrayList;
	/**
	 * 
	 */
	ArrayAdapter<Message> adapter;
	/**
	 * 
	 */
	TextView tvRecipientName;
	/**
	 * 
	 */
	TextView tvRecipient;
	/**
	 * 
	 */
	Button btSend;
	/**
	 * 
	 */
	EditText etMessage;
	/**
	 * 
	 */
	ListView lvMessages;
	/**
	 * 
	 */
	DBHelper db;
	/**
	 * 
	 */
	Contact currentContact;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_sms);

		currentContact = new Contact();

		Bundle b = getIntent().getExtras();
		currentContact.setContactID(b.getLong("id"));
		currentContact.setName(b.getString("name"));
		currentContact.setMobile(b.getLong("mobile"));
		currentContact.setEmail(b.getString("email"));

		db = new DBHelper(getApplicationContext());
		arrayList = db.getMessages(currentContact);

		// arrayList = new ArrayList<Message>();
		adapter = new ArrayAdapter<Message>(this,
				android.R.layout.simple_expandable_list_item_1, arrayList);

		tvRecipientName = (TextView) findViewById(R.id.tvRecipientName);
		tvRecipientName.setText(currentContact.getName());

		tvRecipient = (TextView) findViewById(R.id.tvRecepient);

		tvRecipient.setText("" + currentContact.getMobile());

		btSend = (Button) findViewById(R.id.btSend);
		btSend.setOnClickListener(this);

		etMessage = (EditText) findViewById(R.id.etMessages);

		lvMessages = (ListView) findViewById(R.id.lvMessages);
		lvMessages.setAdapter(adapter);
		lvMessages.setOnItemLongClickListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (v == btSend) {
			Message message = new Message();
			message.setMessage(etMessage.getText().toString());

			long messageID = db.addMessage(currentContact, message);
			message.setMessageID(messageID);

			adapter.add(message);
			adapter.notifyDataSetChanged();

			sendSMS(tvRecipient.getText().toString(), etMessage.getText()
					.toString());

			Utils.notify(this, "Message sent.");

			etMessage.setText("");

		}
	}

	/**
	 * @param number
	 * @param message
	 */
	public void sendSMS(String number, String message) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(number, null, message, null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemLongClickListener#onItemLongClick(android
	 * .widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		Message message = adapter.getItem(arg2);
		
		adapter.remove(message);
		adapter.notifyDataSetChanged();
		db.deleteMessage(message);

		Utils.notify(this, "Message successfully deleted.");

		return true;
	}
}
